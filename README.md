# attocubeIds3010

European Spallation Source ERIC Site-specific EPICS module: attocubeIds3010

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/ESTIA+-+Attocube+IDS-3010+-+Displacement+Measuring+Interferometer)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/attocubeIds3010-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)

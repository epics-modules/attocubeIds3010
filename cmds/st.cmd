# This should be a test startup script
require attocubeids3010

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "172.30.244.83")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "Utgard; $(IPADDR)")
epicsEnvSet(PREFIX, "UTG-SEE-PREMP:Pmonit-PGxx-01")
epicsEnvSet(IOCNAME, "UTG-SEE-PREMP:PG-01")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(attocubeids3010_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

iocshLoad("$(attocubeids3010_DIR)attocubeIds3010.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

iocInit()
